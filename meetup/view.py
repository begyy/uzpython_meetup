from rest_framework.views import APIView, Response


class HelloWorld(APIView):
    def get(self, request):
        return Response({"hello": "version2"})
